/**
 * Binary search tree implementation
 *
 * Copyright (c) 2013 the authors listed at the following URL, and/or
 * the authors of referenced articles or incorporated external code:
 * http://en.literateprograms.org/Binary_search_tree_(C)?action=history&offset=20121127201818
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Retrieved from: http://en.literateprograms.org/Binary_search_tree_(C)?oldid=18734
 * Modified: Nikos Nikoleris <nikos.nikoleris@it.uu.se>
 */


/***********************************************************/
/* NOTE: You can modify/add any piece of code that will    */
/* make your algorithm work                                */
/***********************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <errno.h>
#include <assert.h>
#include "bst.h"

pthread_mutex_t lock;

/**
 * Searches for the node which points to the requested data.
 *
 * @param root        root of the tree
 * @param comparator  function used to compare nodes
 * @param data        pointer to the data to be search for
 * @param parent      put the parent here please
 * @param lock_me     lock the returned item
 * @param lock_parent lock the parent (if any)
 * @return            the node containg the data
 */

char *einval  = "EINVAL";
char *ebusy   = "EBUSY";
char *eagain  = "EAGAIN";
char *edeadlk = "EDEADLK";
char *eperm   = "EPERM";
char *dunno   = "DUNNO";
char *er(int e){
    switch (e) {
    case EINVAL: return einval; break;
    case EBUSY: return ebusy; break;
    case EAGAIN: return eagain; break;
    case EDEADLK: return edeadlk; break;
    case EPERM: return eperm; break;
    default: return dunno;
    }
}

#define eprintf(format, ...)                                            \
    fprintf (stderr, "(%lu | %4i) " format, pthread_self(),  __LINE__, ##__VA_ARGS__); \

#define m_lock(node){                                                   \
        int lock_error;                                                 \
        if ((lock_error = pthread_mutex_lock(&(*node)->node_lock))){    \
            eprintf ("FAIL MUTEX(%p) %s\n", &(*node)->node_lock, er(lock_error)); \
            exit(1);                                                    \
        }                                                               \
    }

#define m_unlock(node){                                                 \
        int lock_error;                                                 \
        if ((lock_error = pthread_mutex_unlock(&(*node)->node_lock))){  \
            eprintf ("FAIL MUTEX(%p) %s\n", &(*node)->node_lock, er(lock_error)); \
            exit(1);                                                    \
        }                                                               \
    }


struct bst_node ** search(struct bst_node** root,
                          comparator compare,
                          void* data,
                          struct bst_node **parent,
                          int lock_me,
                          int lock_parent
    )
{
/* DONE: For the Step 2 you will have to make this function thread-safe */
    struct bst_node** node = root;
    struct bst_node** my_parent = NULL;
    while (*node != NULL) {
        m_lock(node);
        int compare_result = compare(data, (*node)->data);
        if (my_parent != NULL && compare_result != 0){
            m_unlock(my_parent);
        }

        if (compare_result < 0){
            my_parent = node;
            node = &(*node)->left;
        } else if (compare_result > 0) {
            my_parent = node;
            node = &(*node)->right;
        } else {
            break;
        }

    }

    if (lock_parent){
        if (my_parent == NULL)
            *parent = NULL;
        else {
            *parent = *my_parent;
        }
    } else {
        if (my_parent != NULL){
            m_unlock(my_parent);
        }
    }


    if (!lock_me){
        if (*node != NULL){
            m_unlock(node);
        }
    }
    return node;
}


/**
 * Deletes the requested node.
 *
 * @param node       node to be deleted
 */

static void node_delete_aux(struct bst_node** node, struct bst_node** parent)
{
    struct bst_node* old_node = *node;
    if ((*node)->left == NULL && (*node)->right == NULL){
        free_node(old_node);
        *node = NULL;
        if( parent != NULL)
            if(*parent !=NULL)
                m_unlock(parent);
    } else if ((*node)->left == NULL || (*node)->right == NULL) {
        old_node = (*node)->left == NULL ? (*node)->right : (*node)->left;
        m_lock(&old_node);
        (*node)->left  = old_node->left;
        (*node)->right = old_node->right;
        (*node)->data  = old_node->data;
        free_node(old_node);
        m_unlock(node);
        if( parent != NULL)
            if(*parent !=NULL)
                m_unlock(parent);
    }  else {
        struct bst_node** pred = &(*node)->left;
        struct bst_node** old_pred = pred;
        m_lock(pred);
        if( parent != NULL)
            if(*parent !=NULL)
                m_unlock(parent);

        while ((*pred)->right != NULL) {
	    pred = &(*pred)->right;
            m_lock(pred);


            if ((*pred)->right != NULL){
                m_unlock(old_pred);
                old_pred = pred;
            }
	}

	/* Swap values */
	void* temp = (*pred)->data;
	(*pred)->data = (*node)->data;
	(*node)->data = temp;


        if (pred == &(*node)->left){
            parent = node;
        } else {
            m_unlock(node);
            parent = old_pred;
        }

	node_delete_aux(pred, parent);

    }
}

/**
 * Deletes the node which points to the requested data.
 *
 * @param root       root of the tree
 * @param comparator function used to compare nodes
 * @param data       pointer to the data to be deleted
 * @return           1 if data is not found, 0 otherwise
 */
int node_delete(struct bst_node** root, comparator compare, void* data)
{
    struct bst_node** node = search(root, compare, data, NULL, 1, 0);
    if (*node == NULL){
        printf ("LOL\n");
        return -1;
    }
    node_delete_aux(node, NULL);

    return 0;
}

/**
 * Deletes the node which points to the requested data.
 *
 * Should be safe when called in parallel with other threads that
 * might call the same functions. Uses fine grained locking.
 *
 * @param root       root of the tree
 * @param comparator function used to compare nodes
 * @param data       pointer to the data to be deleted
 * @return           1 if data is not found, 0 otherwise
 */
int node_delete_ts_cg(struct bst_node** root, comparator compare, void* data)
{
    /* DONE: Fill-in the body of this function */
    pthread_mutex_lock(&lock);


    struct bst_node** node = search(root, compare, data, NULL, 1, 0);

    if (node == NULL) {
        pthread_mutex_unlock(&lock);
        return -1;
    }
    node_delete_aux(node, NULL);
    pthread_mutex_unlock(&lock);
    return 0;
}

/**
 * Deletes the node which points to the requested data.
 *
 * Should be safe when called pin parallel with other threads that
 * might call the same functions. Uses fine grained locking.
 *
 * @param root       root of the tree
 * @param comparator function used to compare nodes
 * @param data       pointer to the data to be deleted
 * @return           1 if data is not found, 0 otherwise
 */
int node_delete_ts_fg(struct bst_node** root, comparator compare, void* data)
{
    /* DONE: Fill-in the body of this function */
    struct bst_node* parent = NULL;
    struct bst_node** node = search(root, compare, data, &parent, 1, 1);
    if (node == NULL) {
        eprintf ("FAIL\n");
        return -1;
    }
    node_delete_aux(node, &parent);

    return 0;
}


/**
 * Allocate resources and initialize a BST.
 *
 * @return           root of the BST
 */
struct bst_node ** tree_init(void)
{
    struct bst_node** root = malloc(sizeof(*root));
    if (root == NULL) {
        fprintf(stderr, "Out of memory!\n");
        exit(1);
    }
    *root = NULL;

    /* DONE: Initialize any global variables you use for the BST */
    pthread_mutex_init(&lock, NULL);
    return root;
}

/**
 * Remove resources for the tree.
 *
 * @param root       root of the tree
 */
void tree_fini(struct bst_node ** root)
{
    /* DONE: Free any global variables you used for the BST */

    if (root != NULL)
        free(root);

    pthread_mutex_destroy(&lock);
}


/**
 * Inserts a new node with the requested data if not already in the tree.
 *
 * @param root       root of the tree
 * @param comparator function used to compare nodes
 * @param data       pointer to the data to be inserted
 * @return           1 if data is in the BST already, 0 otherwise
 */
int node_insert(struct bst_node** root, comparator compare, void* data)
{

    struct bst_node* parent = NULL;
    struct bst_node** node = search(root, compare, data, &parent, 0, 1);
    if (*node == NULL) {
        *node = new_node(data);
        if (parent != NULL) {
            m_unlock(&parent);
        }
        return 0;
    } else{
        if (parent != NULL) m_unlock(&parent);
        return 1;
    }
}


/**
 * Creates a new node with the requested data.
 *
 * @param data       pointer to the data pointed be the new node
 */
struct bst_node* new_node(void* data)
{
    struct bst_node* node = malloc(sizeof(struct bst_node));
    if (node == NULL) {
        fprintf(stderr, "Out of memory!\n");
        exit(1);
    } else {
        pthread_mutex_init(&node->node_lock, NULL);
        node->left = NULL;
        node->right = NULL;
        node->data = data;
    }

    return node;
}


/**
 * Deletes a node.
 *
 * @param node       node to be freed
 */
void free_node(struct bst_node* node)
{
    if (node == NULL)
        fprintf(stderr, "Invalid node\n");
    else {
        /* NODE: Finalize any per node variables you use for the BST */
        long int owner = ((node)->node_lock).__data.__owner;
        m_unlock(&node);
        int e;
        if((e=pthread_mutex_destroy(&node->node_lock))){
            eprintf("FAILED TO DESTROY (%p)(%li) %s\n", &node->node_lock, owner, er(e));
            exit(1);
        }
        free(node);
    }
}


/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 4
 * indent-tabs-mode: nil
 * c-file-style: "stroustrup"
 * End:
 */
